const dotenv = require("dotenv");
const Discord = require("discord.js");

// this method will read all the variable in env file
dotenv.config();

// create our own client discord
const client = new Discord.Client();

client.on("ready", () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on("message", (msg) => {
  if (msg.content === "ping") {
    msg.reply("pong");
  }
});

// Access to bot application
client.login(process.env.TOKEN);
